import QtQuick 2.15
import QtQuick.Window 2.12

Item {
    id: sCells9
    property color color: "#ffffff"
    property color borderColor: "#000000"
    property int borderWidth: 0
    //

    Rectangle{
        id: mainRect
        color: parent.color
        border.color: parent.borderColor
        border.width: parent.borderWidth
        anchors.fill: parent

        Grid {
            id: grid
            anchors.fill: parent
            anchors.margins: 4
            spacing: 4
            padding: 4
            rows: 3
            columns: 3
            horizontalItemAlignment: Grid.AlignHCenter
            verticalItemAlignment: Grid.AlignVCenter
        }

    }
}

/*##^##
Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
##^##*/
