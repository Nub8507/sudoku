import QtQuick 2.15
import QtQuick.Window 2.12

Window{
//    id: mainWindow
    width: 640
    height: 480
    property alias cell11: cell11
    title: qsTr("Sudoku")

    Rectangle {
        id: mainItem
        anchors.fill: parent
        anchors.rightMargin: 10
        anchors.leftMargin: 10
        anchors.bottomMargin: 10
        anchors.topMargin: 10
        color: "#000000"

        Grid {
            id: grid
            anchors.fill: parent
            spacing: 4
            padding: 4
            rows: 3
            columns: 3
            horizontalItemAlignment: Grid.AlignHCenter
            verticalItemAlignment: Grid.AlignVCenter
            SCells9 { id: cell11; color: "white"; width: (parent.width-4*4)/3; height: (parent.height-4*4)/3 }
            SCells9 { id: cell12; color: "red"; width: (parent.width-4*4)/3; height: (parent.height-4*4)/3   }
            SCells9 { id: cell13; color: "green"; width: (parent.width-4*4)/3; height: (parent.height-4*4)/3 }
            SCells9 { id: cell21; color: "white"; width: (parent.width-4*4)/3; height: (parent.height-4*4)/3 }
            SCells9 { id: cell22; color: "white"; width: (parent.width-4*4)/3; height: (parent.height-4*4)/3 }
            SCells9 { id: cell23; color: "white"; width: (parent.width-4*4)/3; height: (parent.height-4*4)/3 }
            SCells9 { id: cell31; color: "white"; width: (parent.width-4*4)/3; height: (parent.height-4*4)/3 }
            SCells9 { id: cell32; color: "white"; width: (parent.width-4*4)/3; height: (parent.height-4*4)/3 }
            SCells9 { id: cell33; color: "white"; width: (parent.width-4*4)/3; height: (parent.height-4*4)/3 }
        }
    }
}



/*##^##
Designer {
    D{i:0}D{i:1;invisible:true}
}
##^##*/
