#ifndef BASE_DATA_H
#define BASE_DATA_H

#include "base_data_global.h"

#include <math.h>
#include <stdint.h>
#include <list>
#include <vector>

using coord = std::pair<uint8_t,uint8_t>;

class BASE_DATA_EXPORT base_data{

public:
    struct cellValue{

            enum CELL_STATUS{
                CELL_UNKNOWN    =   0,
                CELL_FIX        =   1,
                CELL_USER       =   2,
                CELL_TEST       =   3,
                };

        uint32_t value;
        CELL_STATUS status;
        std::list<uint8_t> possibleList;
        cellValue(const uint8_t maxNum){
            value=0;
            status=CELL_UNKNOWN;
            possibleList.clear();
            for(uint8_t i=1;i<=maxNum;++i)
                possibleList.push_back(i);

        }
        cellValue(uint32_t val, const uint8_t maxNum){
            value=val;
            status=CELL_FIX;
            possibleList.clear();
            for(uint8_t i=1;i<=maxNum;++i){
                if(i!=val){
                    possibleList.push_back(i);
                }
            }

        }
        bool operator==(const cellValue& val){
            if(value==val.value)return true;
            return false;
        }

        bool operator!=(const cellValue& val){
            return !operator==(val);
        }

    };

    explicit base_data(uint8_t size=9);
    ~base_data();

private:

    //
    const uint8_t sideSize;
    const uint8_t subBlockSize;
    const int32_t allSize;
    //
    std::vector<cellValue> values;


    //
    size_t calkPos(coord pos) const {return static_cast<size_t>(pos.first-1)+static_cast<size_t>(pos.second-1)*sideSize;}
    coord pos_to_coord(int pos) const {return coord(pos%sideSize+1,uint8_t(pos/sideSize)+1);}
    uint8_t calk_pos_to_block(coord pos) const {return uint8_t((pos.second-1)/subBlockSize)+uint8_t((pos.first-1)/subBlockSize);}
    bool checkArrayData(const std::vector<cellValue> data, bool full) const;
    bool add_possible(cellValue* cell,uint8_t value);
    bool remove_possible(cellValue* cell,uint8_t value);
    //
    std::vector<cellValue*> get_row_value(uint8_t posY);
    std::vector<cellValue*> get_column_value(uint8_t posX);
    std::vector<cellValue*> get_block_value(uint8_t block_num);
    //

public:

    //
    uint8_t getsideSize() const {return sideSize;}
    //
    bool setValue(coord pos, cellValue::CELL_STATUS status, uint32_t value);
    cellValue getValue(coord pos) const;
    //
    std::vector<cellValue> getRowValue(uint8_t posY) const;
    std::vector<cellValue> getColumnValue(uint8_t posX) const;
    std::vector<cellValue> get_block_value(uint8_t block_num) const;

    //
    bool checkBlockData(uint8_t blokNum,bool full=false) const;
    bool checkRowData(uint8_t posY,bool full=false) const;
    bool checkColumnData(uint8_t posX,bool full=false) const;
    //
    coord find_with_min_possible();
    //

};

#endif // BASEDATA_H
