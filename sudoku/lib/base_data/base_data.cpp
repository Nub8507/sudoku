#include "base_data.h"
#include <algorithm>

base_data::base_data(uint8_t size):
    sideSize(size),
    subBlockSize(static_cast<uint8_t>(sqrt(sideSize))),
    allSize(static_cast<int32_t>(size)*static_cast<int32_t>(size))
{
    //
    for(int i=0;i<=allSize;i++){
        values.push_back(cellValue(sideSize));
    }
    //
}

base_data::~base_data()
{
}

bool base_data::checkArrayData(const std::vector<base_data::cellValue> data, bool full) const
{
    //
    std::vector<uint8_t> rez(sideSize,0);
    //
    for (auto &var: data) {
        if(full ||
            (var.status==cellValue::CELL_FIX || var.status==cellValue::CELL_USER)){
            rez[static_cast<size_t>(var.value-1)]++;
        }
    }
    //
    for(auto &var: rez){
        if(var>1){
            return false;
        }
    }
    //
    return true;
    //
}

bool base_data::add_possible(base_data::cellValue *cell, uint8_t value)
{
    //
    if(value==0)return true;
    //
    cell->possibleList.push_back(value);
    cell->possibleList.unique();
    cell->possibleList.sort();
    return true;
    //
}

bool base_data::remove_possible(base_data::cellValue *cell, uint8_t value)
{
    //
    if(value==0)return true;
    //
    auto rez=std::find(cell->possibleList.begin(),cell->possibleList.end(),value);
    if(rez!=cell->possibleList.end()){
        cell->possibleList.erase(rez);
        if(cell->status==cellValue::CELL_UNKNOWN&&
            cell->possibleList.size()<=0){
            return false;
        }
    }
    return true;
}

bool base_data::setValue(coord pos, cellValue::CELL_STATUS status, uint32_t value)
{
    //
    size_t line_pos=calkPos(pos);
    //
    if(line_pos>=values.size())
        return false;
    //
    if(status==base_data::cellValue::CELL_UNKNOWN){
        values[line_pos].value=value;
        values[line_pos].status=status;
        auto row=get_row_value(pos.second);
        for (auto &val: row){
            add_possible(val,static_cast<uint8_t>(value));
        }
        auto col=get_column_value(pos.first);
        for (auto &val: col){
            add_possible(val,static_cast<uint8_t>(value));
        }
        auto block=get_block_value(calk_pos_to_block(pos));
        for (auto &val: block){
            add_possible(val,static_cast<uint8_t>(value));
        }
    }else{
        if(values.at(line_pos).status!=base_data::cellValue::CELL_UNKNOWN){
            setValue(pos,base_data::cellValue::CELL_UNKNOWN,values.at(line_pos).value);
        }
        values[line_pos].value=value;
        values[line_pos].status=status;
        auto line=get_row_value(pos.second);
        for (auto &val: line){
            if(!remove_possible(val,static_cast<uint8_t>(value))){
                return false;
            }
        }
        auto col=get_column_value(pos.first);
        for (auto &val: col){
            if(!remove_possible(val,static_cast<uint8_t>(value))){
                return false;
            }
        }
        auto block=get_block_value(calk_pos_to_block(pos));
        for (auto &val: block){
            if(!remove_possible(val,static_cast<uint8_t>(value))){
                return false;
            }
        }
    }
    return true;
    //
}

base_data::cellValue base_data::getValue(coord pos) const
{
    //
    size_t line_pos=calkPos(pos);
    //
    if(line_pos>=values.size())
        return cellValue(sideSize);
    //
    return values[line_pos];
    //
}

std::vector<base_data::cellValue> base_data::getRowValue(uint8_t posY) const
{
    //
    std::vector<base_data::cellValue> rez;
    //
    if(posY<=sideSize){
        for(auto i=sideSize*(posY-1);i<sideSize*(posY);i++){
            rez.push_back(values[static_cast<size_t>(i)]);
        }
    }
    //
    return rez;
    //
}

std::vector<base_data::cellValue> base_data::getColumnValue(uint8_t posX) const
{
    //
    std::vector<base_data::cellValue> rez;
    //
    if(posX<=sideSize){
        for(auto i=static_cast<int>(posX-1);i<allSize;i+=sideSize){
            rez.push_back(values[static_cast<size_t>(i)]);
        }
    }
    //
    return rez;
    //
}

std::vector<base_data::cellValue> base_data::get_block_value(uint8_t block_num) const
{
    //
    int pos=int(block_num%subBlockSize)*subBlockSize;
    pos+=int(block_num/subBlockSize)*(subBlockSize*sideSize);
    //
    std::vector<base_data::cellValue> rez;
    //
    for(int i=0;i<subBlockSize*subBlockSize;++i){
        rez.push_back(values[static_cast<size_t>(pos+i%subBlockSize+int(i/subBlockSize)*subBlockSize)]);
    }
    //
    return rez;
    //
}

std::vector<base_data::cellValue*> base_data::get_row_value(uint8_t posY)
{
    //
    std::vector<base_data::cellValue*> rez;
    //
    if(posY<=sideSize){
        for(auto i=sideSize*(posY-1);i<sideSize*(posY);i++){
            rez.push_back(&values[static_cast<size_t>(i)]);
        }
    }
    //
    return rez;
    //
}

std::vector<base_data::cellValue*> base_data::get_column_value(uint8_t posX)
{
    //
    std::vector<base_data::cellValue*> rez;
    //
    if(posX<=sideSize){
        for(auto i=static_cast<int>(posX-1);i<allSize;i+=sideSize){
            rez.push_back(&values[static_cast<size_t>(i)]);
        }
    }
    //
    return rez;
    //
}

std::vector<base_data::cellValue*> base_data::get_block_value(uint8_t block_num)
{
    //
    int pos=int(block_num%subBlockSize)*subBlockSize;
    pos+=int(block_num/subBlockSize)*(subBlockSize*sideSize);
    //
    std::vector<base_data::cellValue*> rez;
    //
    for(int i=0;i<subBlockSize*subBlockSize;++i){
        auto d1=static_cast<size_t>(pos+i%subBlockSize+int(i/subBlockSize)*sideSize);
        auto d2=i%subBlockSize;
        auto d3=int(i/subBlockSize);
        auto d4=pos+i%subBlockSize;
        auto d5=int(i/subBlockSize)*sideSize;
        rez.push_back(&values[static_cast<size_t>(pos+i%subBlockSize+int(i/subBlockSize)*sideSize)]);
    }
    //
    return rez;
    //
}

bool base_data::checkBlockData(uint8_t blokNum, bool full) const
{
    //
    auto rez=get_block_value(blokNum);
    //
    return checkArrayData(rez,full);
    //
}

bool base_data::checkRowData(uint8_t posY, bool full) const
{
    //
    return checkArrayData(getRowValue(posY),full);
    //
}

bool base_data::checkColumnData(uint8_t posX, bool full) const
{
    return checkArrayData(getColumnValue(posX),full);
}

coord base_data::find_with_min_possible()
{
    //
    auto min_index=-1;
    size_t min_pos=sideSize+1;
    //
    for (size_t i=0;i<values.size();++i) {
        //
        if(values[i].status!=cellValue::CELL_UNKNOWN) continue;
        //
        if(values[i].possibleList.size()<=0){
            return pos_to_coord(static_cast<int>(i));
        }
        if(min_pos>values[i].possibleList.size()){
            min_pos=values[i].possibleList.size();
            min_index=static_cast<int>(i);
        }
    }
    //
    if(min_index==-1){
        return {0,0};
    }
    return pos_to_coord(min_index);
    //
}

