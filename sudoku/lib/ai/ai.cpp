#include "ai.h"

bool AI::find_result()
{
    //
    auto cor=game_data.find_with_min_possible();
    const coord t={0,0};
    if(cor==t)return true;
    //
    do{
        if(fill_possible(cor)){
            filled_pos.push_back(cor);
            cor=game_data.find_with_min_possible();
        }else{
            if(filled_pos.empty()) return false;
            //
            cor=filled_pos.back();
            filled_pos.pop_back();
        }
    }while(cor!=t);
    //
    return true;
    //
}

bool AI::fill_possible(const coord pos)
{
    //
    auto value=calk_enabled_value(pos);
    if (value==0){
        return false;
    }else{
        return game_data.setValue(pos,base_data::cellValue::CELL_TEST,value);
    }
    //
}

uint8_t AI::calk_enabled_value(const coord pos)
{
    //
    auto val=game_data.getValue(pos);
    uint8_t value=0;
    if(val.status==base_data::cellValue::CELL_UNKNOWN){
        value=*val.possibleList.begin();
    }else{
        for (auto &f_val: val.possibleList) {
            if(f_val>val.value){
                return f_val;
            }
        }
    }
    return value;
    //
}
