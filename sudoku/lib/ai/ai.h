#ifndef AI_H
#define AI_H
#include "ai_global.h"
#include "base_data.h"

class AI_EXPORT AI{
public:
    AI(base_data& data):game_data(data){}

    bool find_result();

private:

    base_data &game_data;
    std::list<coord> filled_pos;


    bool fill_possible(const coord pos);
    uint8_t calk_enabled_value(const coord pos);


};

#endif // AI_H
