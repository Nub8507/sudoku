#ifndef LOGIC_H
#define LOGIC_H

#include "ai.h"
#include "base_data.h"

#include <QObject>

class Logic : public QObject
{
    Q_OBJECT
public:
    Logic(QObject *parent = nullptr);

    void test();
signals:

public slots:

private slots:

private:

    base_data game_data;
    AI* ai;

};

#endif // LOGIC_H
