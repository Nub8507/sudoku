cmake_minimum_required(VERSION 3.15)

project(sTest LANGUAGES CXX)
enable_testing()

include_directories(../sudoku/lib/ai)
include_directories(../sudoku/lib/base_data)

find_package(QT NAMES Qt6 Qt5 COMPONENTS Test Gui REQUIRED)
find_package(Qt${QT_VERSION_MAJOR} COMPONENTS Test Gui REQUIRED)

set(CMAKE_INCLUDE_CURRENT_DIR ON)

set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED ON)


add_executable(sTest tst_stest.cpp)
add_test(sTest COMMAND sTest)

target_link_libraries(sTest PRIVATE Qt${QT_VERSION_MAJOR}::Gui Qt${QT_VERSION_MAJOR}::Test base_data ai)

