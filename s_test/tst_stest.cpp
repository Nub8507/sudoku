#include <QtTest>

#include "base_data.h"

class sTest : public QObject
{
    Q_OBJECT

public:
    sTest();
    ~sTest();

private slots:
    void initTestCase();
    void cleanupTestCase();
    void test_case1();
    void test_case2();

private:
    base_data* m_data;

};

sTest::sTest()
{
    auto t=0;
    t++;
}

sTest::~sTest()
{
}

void sTest::initTestCase()
{
    m_data=new base_data();
}

void sTest::cleanupTestCase()
{
    delete m_data;
}

void sTest::test_case1()
{
//    QCOMPARE(m_data->getValue({0,0}).value,0);
}

void sTest::test_case2()
{

}

QTEST_APPLESS_MAIN(sTest)

#include "tst_stest.moc"
